//
//  CategoryViewController.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/29/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit
import CoreData

class CategoryViewController: UITableViewController {
    
    let options = [
        "Want to Read",
        "Reading Now",
        "Have Read"
    ]
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var selectedIndexPath: IndexPath!
    var item: BookItem!
    var bookID: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        navigationItem.rightBarButtonItem?.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        //presentedViewController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        //presentedViewController?.dismiss(animated: true, completion: nil)
        if bookID != nil {
            let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
            let predicate = NSPredicate(format: "bookID = '\(bookID!)'")
            fetchRequest.predicate = predicate
            var results: [NSManagedObject] = []
            do {
                results = try PersistenceService.context.fetch(fetchRequest)
            } catch {
                print("Error executing fetch request: \(error)")
            }
            if results.count == 1 {
                let objectUpdate = results[0]
                objectUpdate.setValue(options[selectedIndexPath.row], forKey: "tag")
                do {
                    try PersistenceService.context.save()
                } catch {
                    print("Error while saving: \(error)")
                }
            }
        } else {
            let book = Book(context: PersistenceService.context)
            book.bookID = item.bookID
            book.title = item.title
            book.author = item.author
            book.bookDescription = item.description
            book.averageRating = item.averageRating ?? 0
            book.totalRatings = Int16(item.totalRatings ?? 0)
            book.genres = item.genres.joined(separator: ", ")
            book.releaseDate = dateFormatter.date(from: item.releaseDate.substring(to: 10))! as NSDate
            book.imgUrl = item.imgUrl
            book.tag = options[selectedIndexPath.row]
            PersistenceService.saveContext()
            print("Book description: \(book.description)")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CategoryViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if let index = selectedIndexPath, index == indexPath {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.textLabel?.text = options[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !(navigationItem.rightBarButtonItem?.isEnabled)! {
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        selectedIndexPath = indexPath
        print(tableView.indexPathForSelectedRow?.row ?? -1)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}
