//
//  BookSearchTable.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/20/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit

class BookSearchTable: UITableViewController {
    
    var matchingItems = [BookSearch]()
    
}

extension BookSearchTable: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        
        self.tableView.reloadData()
    }
}

extension BookSearchTable {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let selectedItem = matchingItems[indexPath.row]
        cell.textLabel?.text = selectedItem.title
        cell.detailTextLabel?.text = selectedItem.author
        return cell
    }
}
