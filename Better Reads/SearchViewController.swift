//
//  ViewController.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/19/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    

    @IBOutlet weak var collectionView: UICollectionView!
    
    var items = [BookItem]()
    
    // Init a SearchController, which contains the search bar
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = "Search"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // Provide collectionView with necessary data source and delegate, and register the custom cell
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "BookCell", bundle: nil), forCellWithReuseIdentifier: "BookCell")
        
        
        // Set up the search bar
//        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        //searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.delegate = self
        definesPresentationContext = true // !IMPORTANT
        
        // Configure the search bar
        let searchBar = searchController.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Apple Books store"
        
        //Add the search bar to the nav bar's title
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        //Remove the title bar bottom border
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showDetailFromSearch"?:
            print("showDetailFromSearch")
            if let selectedIndexPath = collectionView.indexPathsForSelectedItems?.first {
                let book = items[selectedIndexPath.row]
                let destinationVC = segue.destination as! BookInfoViewController
                destinationVC.bookItem = book
            }
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
    
    func fetchSearchResults() {
        
        self.items = []
        self.collectionView.reloadData()
        
        let searchTerm = navigationItem.searchController?.searchBar.text ?? ""
        if !searchTerm.isEmpty {
            
            // set up query dictionary
            let queries = [
                "term":searchTerm,
                "media":"ebook",
                "limit":"20",
                "lang":"en_US"
            ]
            
            BookAPI.performSearch(with: queries, completion: { (bookItems) in
                DispatchQueue.main.async {
                    if let books = bookItems {
                        print("Received and decoded the data")
                        self.items = books
                        self.collectionView.reloadData()
                    } else {
                        print("Unable to search")
                    }
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            })
        }
    }
}

extension SearchViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookCell", for: indexPath) as! BookCell
        cell.configure(with: items[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetailFromSearch", sender: self)
    }
}

extension SearchViewController: UISearchControllerDelegate, UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        fetchSearchResults()
        searchController.dimsBackgroundDuringPresentation = false
        //searchController.isActive = false
    }
    
    
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        let cellWidth = (collectionViewWidth - 10) / 2
        return CGSize(width: cellWidth, height: cellWidth * 0.8)
    }
}





