//
//  BookAPI.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/20/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit

struct BookSearch: Decodable {
    let title: String
    let author: String
    
    enum CodingKeys : String, CodingKey {
        case title = "trackName"
        case author = "artistName"
    }
}

struct Result: Decodable {
    let results: [BookSearch]
}



class BookAPI {
    
    // UNFINISHED! DO NOT USE
//    public static func searchSuggestion(term keyword: String) -> [BookSearch]{
//        var suggestions: [BookSearch] = []
//
//        var components = URLComponents(string: "https://itunes.apple.com/search")!
//
//        var queryItems = [URLQueryItem]()
//
//        let baseParams = [
//            "term": keyword,
//            "media": "ebook",
//            "limit": "5"
//        ]
//
//        for (key, value) in baseParams {
//            let item = URLQueryItem(name: key, value: value)
//            queryItems.append(item)
//        }
//        components.queryItems = queryItems
//
//        URLSession.shared.dataTask(with: components.url!) { (data, response, error) in
//            guard let data = data else {
//                print("Error: No data to decode")
//                if let error = error {
//                    print(error.localizedDescription)
//                }
//                return
//            }
//
//            guard let result = try? JSONDecoder().decode(Result.self, from: data)  else {
//                print("Error: Couldn't decode data into Blog")
//                if let error = error {
//                    print(error.localizedDescription)
//                }
//                return
//            }
//            for dict in result.results {
//                suggestions.append(dict)
//            }
//
//            }.resume()
//        print(suggestions.count)
//        return suggestions
//    }
    
    public static func performSearch (with queryItems: [String:String], completion: @escaping ([BookItem]?) -> Void) {
        let baseURL = URL(string: "https://itunes.apple.com/search")
        
        guard let url = baseURL?.withQueries(queryItems) else {
            completion(nil)
            print("Unable to construct a url from the provided queries")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                do {
                    let bookItems = try JSONDecoder().decode(BookItems.self, from: data)
                    completion(bookItems.results)
                } catch let err {
                    print(err)
                }
            } else {
                print("No data was returned")
                completion(nil)
                return
            }
        }.resume()
    }
}
