//
//  HeaderView.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/30/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit

class HeaderView: UICollectionReusableView {      
   
    @IBOutlet weak var sectionHeader: UILabel!
    @IBOutlet weak var sectionButton: UIButton!
}
