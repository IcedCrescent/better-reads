//
//  String+Helpers.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/29/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import Foundation

import Foundation

extension String{
    /// Returns a new string containing the characters of the String from the one at a given index to the end. If 'from' exceeds endIndex it returns an empty string.
    func substring(from: Int) -> String {
        guard let offsetStartIndex = index(startIndex, offsetBy: from, limitedBy: endIndex) else { return "" }
        return String(self[offsetStartIndex..<endIndex])
    }
    
    /// Returns a new string containing the characters of the String up to the one at a given index. If 'to' exceeds endIndex it returns the string as it was.
    func substring(to: Int) -> String {
        return String(self[startIndex..<(index(startIndex, offsetBy: to, limitedBy: endIndex) ?? endIndex)])
    }
    
    /// Returns a string object containing the characters of the String that lie within a given range. If 'from' exceeds endIndex it returns an empty string. If 'to' exceeds endIndex it returns the string from 'from' to endIndex.
    func substring(from: Int, to: Int) -> String {
        guard let offsetStartIndex = index(startIndex, offsetBy: from, limitedBy: endIndex) else { return "" }
        guard let offsetEndIndex = index(startIndex, offsetBy: to, limitedBy: endIndex) else { return String(self[offsetStartIndex..<endIndex]) }
        return String(self[offsetStartIndex..<offsetEndIndex])
    }
}
