//
//  BookInfoViewController.swift
//  Better Reads
//
//  Created by HuyNN.local on 10/26/18.
//  Copyright © 2018 FSoft. All rights reserved.
//

import UIKit
import CoreData

class BookInfoViewController: UIViewController {
    
    @IBOutlet var bookTitle: UILabel!
    @IBOutlet var bookAuthor: UILabel!
    @IBOutlet var bookDescription: UILabel!
    @IBOutlet var bookImage: UIImageView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var averageRating: UILabel!
    @IBOutlet weak var userRatings: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var genres: UILabel!
    
    var bookItem: BookItem!
    var book: Book!
    
    let numberFormater: NumberFormatter = {
        let f = NumberFormatter()
        f.usesGroupingSeparator = true
        f.numberStyle = .decimal
        f.locale = NSLocale.current
        return f
    }()
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    func existsInCoreData(id: String) -> Bool {
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "bookID = '\(id)'")
        
        var results = [NSManagedObject]()
        
        do {
            results = try PersistenceService.context.fetch(fetchRequest)
        } catch {
            print("Error executing fetch request: \(error)")
        }
        return results.count > 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let height = button.bounds.height
        button.layer.cornerRadius = height / 2
        button.contentEdgeInsets = UIEdgeInsetsMake(5,10,5,10)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navController = self.navigationController, navController.viewControllers.count >= 2 {
            let viewController = navController.viewControllers[navController.viewControllers.count - 2]
            if (viewController is SearchViewController) {
                if existsInCoreData(id: bookItem.bookID) {
                    button.setTitle("Update", for: .normal)
                } else {
                    button.setTitle("Add", for: .normal)
                }
            } else {
                button.setTitle("Update", for: .normal)
            }
        }
        
        if self.isBeingPresented || self.isMovingToParentViewController {
            // Perform an action that will only be done once
            navigationItem.largeTitleDisplayMode = .never
            if book != nil {
                bookTitle.text = book.title
                bookAuthor.text = book.author
                bookDescription.text = book.bookDescription
                averageRating.text = "\(book.averageRating)/5"
                userRatings.text = "\(numberFormater.string(from: (book.totalRatings) as NSNumber)!)"
                dateFormatter.dateFormat = "MMM dd, yyyy"
                releaseDate.text = dateFormatter.string(from: book.releaseDate! as Date)
                genres.text = book.genres
                bookImage.image = #imageLiteral(resourceName: "BookStack")
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                URLSession.shared.dataTask(with: book.imgUrl!) { (data, response, error) in
                    DispatchQueue.main.async {
                        if let data = data, let image = UIImage(data: data) {
                            self.bookImage.image = image
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    }.resume()
            } else {
                bookTitle.text = bookItem.title
                bookAuthor.text = bookItem.author
                bookDescription.text = bookItem.description
                averageRating.text = "\(bookItem.averageRating ?? 0)/5"
                userRatings.text = "\(numberFormater.string(from: (bookItem.totalRatings ?? 0) as NSNumber)!)"
                let parsedDate = dateFormatter.date(from: bookItem.releaseDate.substring(to: 10))!
                dateFormatter.dateFormat = "MMM dd, yyyy"
                releaseDate.text = dateFormatter.string(from: parsedDate)
                //genres.text = "\(bookItem.genres.first!), \(bookItem.genres.count > 1 ? "..." : "")"
                genres.text = bookItem.genres.joined(separator: ", ")
                bookImage.image = #imageLiteral(resourceName: "BookStack")
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                URLSession.shared.dataTask(with: bookItem.imgUrl) { (data, response, error) in
                    DispatchQueue.main.async {
                        if let data = data, let image = UIImage(data: data) {
                            self.bookImage.image = image
                        }
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    }.resume()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        let destinationNavigationController = segue.destination as! UINavigationController
        let targetController = destinationNavigationController.topViewController as! CategoryViewController
        if book != nil {
            targetController.bookID = book.bookID
            targetController.navigationItem.title = "Update"
        } else {
            if existsInCoreData(id: bookItem.bookID) {
                targetController.bookID = bookItem.bookID
                targetController.navigationItem.title = "Update"
            } else {
                targetController.item = bookItem
                targetController.navigationItem.title = "Add to My Books"
            }
        }
     }
}
